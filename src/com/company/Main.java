package com.company;




import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {


        System.out.println();
        System.out.println();
        System.out.println();


        List<TechLead> TL = new ArrayList<TechLead>() {
            {

                add(new TechLead(204, 95558745, "Pratham", "Android"));
                add(new TechLead(206, 95558745, "Ghjhds", "EAS"));

            }
        };
        List<Director> Dr = new ArrayList<Director>() {
            {

                add(new Director(301, 874562, "adsf", "Android"));

                add(new Director(302, 874562, "", "Android"));

            }
        };
        List<SSE> sse = new ArrayList<SSE>() {
            {

                add(new SSE(401, 874562, "adsf", "Android"));

                add(new SSE(403, 874562, "fdjhkj", "IOS"));

            }
        };
        List<ASE> ase = new ArrayList<ASE>() {
            {

                add(new ASE(501, 874562, "asdh", "Android"));

                add(new ASE(502, 874562, "fdjhkj", "IOS"));

            }
        };


        System.out.println("**********************Welcome to YML***************************");
        System.out.println("***************************************************************");
        System.out.println();
        System.out.println();
        System.out.println();


        System.out.println();
        System.out.println();
        System.out.println();



        System.out.println("E ID" + "\t" + "PhoneNumber" + "\t\t" + "Name" + "\t\t" + "Department" + "\t" + "Designation");


        for(int i=0;i< TL.size();i++)
            TL.get(i).displayInfo();

        System.out.println();
        System.out.println();
        System.out.println();


        for(int i=0;i< sse.size();i++)
            sse.get(i).displayInfo();

        System.out.println();
        System.out.println();
        System.out.println();

        for(int i=0;i< ase.size();i++)
            ase.get(i).displayInfo();

        System.out.println();
        System.out.println();
        System.out.println();

        for(int i=0;i< Dr.size();i++)
            Dr.get(i).displayInfo();


            System.out.println();
            System.out.println();
            System.out.println();

            System.out.println();
            System.out.println("*******************************************************************");


        }

    }


abstract class YML{

    int E_id;
    long PhoneNumber;
    String Name,Dept;

 abstract void displayInfo();


}


class TechLead extends YML{

    TechLead(int E_id,long PhoneNumber,String Name, String Dept){

        this.E_id = E_id;
        this.PhoneNumber = PhoneNumber;
        this.Name = Name;
        this.Dept = Dept;

    }

    void displayInfo(){

        System.out.println(E_id+"\t\t"+PhoneNumber+"\t\t"+Name+"\t\t"+Dept+"\t\t"+"TechLead");

    }
}

class SSE extends YML{

    SSE(int E_id,long PhoneNumber,String Name, String Dept){

        this.E_id = E_id;
        this.PhoneNumber = PhoneNumber;
        this.Name = Name;
        this.Dept = Dept;

    }

    void displayInfo(){

        System.out.println(E_id+"\t\t"+PhoneNumber+"\t\t\t"+Name+"\t\t"+Dept+"\t\t"+"SSE");

    }
}

class Director extends YML{

    Director(int E_id,long PhoneNumber,String Name, String Dept){

        this.E_id = E_id;
        this.PhoneNumber = PhoneNumber;
        this.Name = Name;
        this.Dept = Dept;

    }

    void displayInfo(){

        System.out.println(E_id+"\t\t"+PhoneNumber+"\t\t\t"+Name+"\t\t"+Dept+"\t\t"+"Director");

    }
}

class ASE extends YML{

    ASE(int E_id,long PhoneNumber,String Name, String Dept){

        this.E_id = E_id;
        this.PhoneNumber = PhoneNumber;
        this.Name = Name;
        this.Dept = Dept;

    }

    void displayInfo(){

        System.out.println(E_id+"\t\t"+PhoneNumber+"\t\t\t"+Name+"\t\t"+Dept+"\t\t"+"ASE");

    }
}
